/* - Force theater mode - C1 */
function forceTheaterMode(){
	let theaterModeButton = document.getElementsByClassName('ytp-size-button')[0];
	if(theaterModeButton.getElementsByTagName("path")[0].getAttribute("d") === "m 28,11 0,14 -20,0 0,-14 z m -18,2 16,0 0,10 -16,0 0,-10 z"){
		theaterModeButton.click();
	}
}
setInterval(forceTheaterMode, 500);


/* - Replace the subscription list from the side menu by a link to the subscription manager - U3 */
function replaceSubscriptionManager(){
	if(document.getElementById("avatar-btn") || document.getElementById("yt-masthead-account-picker")){
		let subGuide = 
			document.getElementsByTagName("ytd-guide-section-renderer").length ? 
				document.getElementsByTagName("ytd-guide-section-renderer")[1] : 
				document.getElementById("guide-subscriptions-section");
				
		let subManagerLink = document.createElement('a');
		subManagerLink.setAttribute('href', 'https://www.youtube.com/subscription_manager');
		subGuide.getElementsByTagName("div")[0].style.display="none";
		
		let subGuideTitle = subGuide.getElementsByTagName("h3")[0];
		
		if(subGuideTitle.getElementsByTagName("yt-formatted-string").length > 0){
			subGuideTitle.getElementsByTagName("yt-formatted-string")[0].style.textTransform="capitalize";
		} else{
			subGuideTitle.style.textTransform = "capitalize";
			subGuideTitle.style.color = "dimgrey";
		}
		
		subManagerLink.appendChild(subGuideTitle);
		
		if(subGuide.getElementsByTagName("hr").length){
			subGuide.insertBefore(subManagerLink, subGuide.getElementsByTagName("hr")[0]);
		} else {
			subGuide.appendChild(subManagerLink);
		}
	}
}

window.addEventListener("load",replaceSubscriptionManager);
document.getElementById("appbar-guide-button").addEventListener("click",replaceSubscriptionManager);
document.getElementById("guide-button").addEventListener("click",replaceSubscriptionManager);
replaceSubscriptionManager();

