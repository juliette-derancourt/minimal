#include "functions.h"

std::vector<std::string> split(const std::string stringToSplit, const std::string delimiter) {
  std::vector<std::string> splits;
  size_t cursor = 0;
  size_t lastCursor = 0;
  const int delimiterLength = delimiter.length();
  while ((cursor = stringToSplit.find(delimiter, lastCursor)) != std::string::npos) {
    splits.push_back(stringToSplit.substr(lastCursor, cursor-lastCursor));
    lastCursor = cursor + delimiterLength;
  }
  splits.push_back(stringToSplit.substr(lastCursor, std::string::npos));
  return splits;
}

void replace(std::string& stringToParse, const std::string stringToFind, const std::string newString) {
  size_t positionFound = stringToParse.find(stringToFind);
  if (positionFound != std::string::npos) {
    stringToParse.replace(positionFound, stringToFind.length(), newString);
  }
}

bool isFile(const std::string path) {
  std::ifstream fileToCheck(path);
  return !fileToCheck.fail();
}

std::string capitalize(std::string stringToCap) {
  if(stringToCap.length() > 0){
    stringToCap[0] = std::toupper(stringToCap[0]);
  }
  return stringToCap;
}

