all: development

development: manifest_dev.json clean zip_dev

test: development
	firefox --new-tab about:debugging#/runtime/this-firefox

release: manifest.json clean zip

zip:
	zip -r ./minimal.zip LICENSE README.md CHANGES.md manifest.json icons scripts styles

zip_dev:
	mv manifest.json manifest_release.json
	mv manifest_dev.json manifest.json
	zip -r ./minimal_dev.zip LICENSE README.md CHANGES_dev.md manifest.json icons_dev scripts styles
	mv manifest.json manifest_dev.json
	mv manifest_release.json manifest.json

manifest.json: preprocessing/preprocess $(shell find data -type f) $(shell find scripts -type f) $(shell find styles -type f)
	./preprocessing/preprocess release

manifest_dev.json: preprocessing/preprocess $(shell find data -type f) $(shell find scripts -type f) $(shell find styles -type f)
	./preprocessing/preprocess development

.PHONY: ppmake

ppmake: preprocessing/preprocess

preprocessing/preprocess: 
	cd preprocessing && $(MAKE)

version:
	cat ./data/version

clean:
	$(RM) ./minimal.zip ./minimal_dev.zip

ppclean:
	cd preprocessing && $(MAKE) clean
	
deepclean: clean ppclean
